// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X
Shader "DispBaseHTA"
{
	Properties
	{
		// _TessValue( "Max Tessellation", Range( 1, 32 ) ) = 10
		// _TessMin( "Tess Min Distance", Float ) = -4
		// _TessMax( "Tess Max Distance", Float ) = 40
		_TessPhongStrength( "Phong Tess Strength", Range( 0, 1 ) ) = 0.5
		// _aSMA("DAPI R2", 2D) = "black" {}
		// _CD45("CD45", 2D) = "black" {}
		// _pHH3("pHH3", 2D) = "black" {}
		// _CD31("DAPI R3", 2D) = "black" {}
		// _CD4("CD4", 2D) = "black" {}
		// _LaminAC("DAPI R4", 2D) = "black" {}
		// _DAPI_R5("DAPI R5", 2D) = "black" {}
		// _CD68("CD68", 2D) = "black" {}
		// _DAPI("DAPI", 2D) = "black" {}
		// _Ki67("Ki67", 2D) = "black" {}
		// _Her2("Her2", 2D) = "black" {}
		// _Estrogen_Receptor("Estrogen_Receptor", 2D) = "black" {}
		// _Vimentin("Vimentin", 2D) = "black" {}
		// _CK19("CK19", 2D) = "black" {}
		// _PCNA("PCNA", 2D) = "black" {}
		// _CD8("CD8", 2D) = "black" {}
		// DispMap("Disp Map", 2D) = "black" {}
		// DispMap_Scale("Disp Map Scale", Range(0.001, 1)) = 0.5
		// _aSmA("aSmA", 2D) = "black" {}
		[HideInInspector] _Black("Black", 2D) = "black" {}
		// _CK7("CK7", 2D) = "black" {}
		// _Ecadherin("Ecadherin", 2D) = "black" {}
		[HideInInspector] _AAAAAAAAAAAA("AAAAAAAAAAAA", 2DArray) = "" {}
		// DispMap_On("DispMap_On", Float) = 0
		// [HideInInspector] aSMA_On("aSMA_On", Float) = 1
		// [HideInInspector] Her2_On("Her2_On", Float) = 0
		// [HideInInspector] CD4_On("CD4_On", Float) = 0
		// [HideInInspector] CD8_On("CD8_On", Float) = 0
		// [HideInInspector] CD31_On("CD31_On", Float) = 1
		// [HideInInspector] CD45_On("CD45_On", Float) = 0
		// [HideInInspector] CD68_On("CD68_On", Float) = 0
		// [HideInInspector] CK7_On("CK7_On", Float) = 0
		// [HideInInspector] CK19_On("CK19_On", Float) = 0
		// [HideInInspector] DAPI_On("DAPI_On", Float) = 0
		// [HideInInspector] Ecadherin_On("Ecadherin_On", Float) = 0
		// [HideInInspector] Estrogen_Receptor_On("Estrogen_Receptor_On", Float) = 0
		// [HideInInspector] Ki67_On("Ki67_On", Float) = 0
		// [HideInInspector] LaminAC_On("LaminAC_On", Float) = 1
		// [HideInInspector] PCNA_On("PCNA_On", Float) = 0
		// [HideInInspector] pHH3_On("pHH3_On", Float) = 0
		// [HideInInspector] Vimentin_On("Vimentin_On", Float) = 0
		// [HideInInspector] DAPI_R5_On("DAPI_R5_On", Float) = 1
		// [HideInInspector] aSmA_On("aSmA_On", Float) = 0
		// aSMA_Color("aSMA_Color", Color) = (0, 1, 1, 1)
		// [HideInInspector] Her2_Color("Her2_Color", Color) = (0, 1, 1, 1)
		// [HideInInspector] CD4_Color("CD4_Color", Color) = (1, 1, 0, 1)
		// [HideInInspector] CD8_Color("CD8_Color", Color) = (1, 0, 0, 1)
		// [HideInInspector] CD31_Color("CD31_Color", Color) = (0, 0, 1, 1)
		// [HideInInspector] CD45_Color("CD45_Color", Color) = (1, 0, 1, 1)
		// [HideInInspector] CD68_Color("CD68_Color", Color) = (0, 1, 0, 1)
		// [HideInInspector] CK7_Color("CK7_Color", Color) = (1, 1, 0, 1)
		// [HideInInspector] CK19_Color("CK19_Color", Color) = (0, 0, 1, 1)
		// [HideInInspector] DAPI_Color("DAPI_Color", Color) = (1, 0, 0, 1)
		// [HideInInspector] Ecadherin_Color("Ecadherin_Color", Color) = (0, 0, 1, 1)
		// [HideInInspector] Estrogen_Receptor_Color("Estrogen_Receptor_Color", Color) = (1, 0, 0, 1)
		// [HideInInspector] Ki67_Color("Ki67_Color", Color) = (1, 0, 1, 1)
		// [HideInInspector] LaminAC_Color("LaminAC_Color", Color) = (0, 1, 1, 1)
		// [HideInInspector] PCNA_Color("PCNA_Color", Color) = (0, 1, 0, 1)
		// [HideInInspector] pHH3_Color("pHH3_Color", Color) = (0, 0, 1, 1)
		// [HideInInspector] Vimentin_Color("Vimentin_Color", Color) = (1, 0, 1, 1)
		// [HideInInspector] DAPI_R5_Color("DAPI_R5_Color", Color) = (1, 1, 0, 1)
		// [HideInInspector] aSmA_Color("aSmA_Color", Color) = (0, 0, 1, 1)
		[HideInInspector] _texcoord( "", 2D ) = "black" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull off
		CGPROGRAM
		#include "Tessellation.cginc"
		#pragma target 5.0
		#pragma exclude_renderers vulkan xbox360 xboxone ps4 psp2 n3ds wiiu
		#pragma surface surf Lambert keepalpha addshadow fullforwardshadows noshadow vertex:vertexDataFunc tessellate:tessFunction tessphong:_TessPhongStrength
		#pragma require 2darray
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform float DispMap_On;
		uniform sampler2D DispMap;
		uniform float4 DispMap_ST;
		uniform float DispMap_Scale;

		uniform float aSMA_On;
		uniform float4 aSMA_Color;
		// uniform sampler2D _aSMA;


		uniform float Her2_On;
		uniform float4 Her2_Color;
		// uniform sampler2D _Her2;


		uniform float CD4_On;
		uniform float4 CD4_Color;
		// uniform sampler2D _CD4;


		uniform float CD8_On;
		uniform float4 CD8_Color;
		// uniform sampler2D _CD8;


		uniform float CD31_On;
		uniform float4 CD31_Color;
		// uniform sampler2D _CD31;


		uniform float CD45_On;
		uniform float4 CD45_Color;
		// uniform sampler2D _CD45;


		uniform float CD44_On;
		uniform float4 CD44_Color;
		// uniform sampler2D _CD44;


		uniform float CD68_On;
		uniform float4 CD68_Color;
		// uniform sampler2D _CD68;


		uniform float CK7_On;
		uniform float4 CK7_Color;
		// uniform sampler2D _CK7;


		uniform float CK19_On;
		uniform float4 CK19_Color;
		// uniform sampler2D _CK19;


		uniform float DAPI_On;
		uniform float4 DAPI_Color;
		// uniform sampler2D _DAPI;


		uniform float Ecadherin_On;
		uniform float4 Ecadherin_Color;
		// uniform sampler2D _Ecadherin;


		uniform float Estrogen_Receptor_On;
		uniform float4 Estrogen_Receptor_Color;
		// uniform sampler2D _Estrogen_Receptor;


		uniform float Ki67_On;
		uniform float4 Ki67_Color;
		// uniform sampler2D _Ki67;


		uniform float LaminAC_On;
		uniform float4 LaminAC_Color;
		// uniform sampler2D _LaminAC;


		uniform float PCNA_On;
		uniform float4 PCNA_Color;
		// uniform sampler2D _PCNA;


		uniform float pHH3_On;
		uniform float4 pHH3_Color;
		// uniform sampler2D _pHH3;


		uniform float Vimentin_On;
		uniform float4 Vimentin_Color;
		// uniform sampler2D _Vimentin;



		uniform float _TessValue;
		uniform float _TessMin;
		uniform float _TessMax;
		uniform float _TessPhongStrength;
		UNITY_DECLARE_TEX2DARRAY(_AAAAAAAAAAAA);

		float3 RGBToHSV(float3 c)
		{
			float4 K = float4(0.0f, -1.0f / 3.0f, 2.0f / 3.0f, -1.0f);
			float4 p = lerp( float4( c.bg, K.wz ), float4( c.gb, K.xy ), step( c.b, c.g ) );
			float4 q = lerp( float4( p.xyw, c.r ), float4( c.r, p.yzx ), step( p.x, c.r ) );
			float d = q.x - min( q.w, q.y );
			float e = 1.0e-10;
			return float3( abs(q.z + (q.w - q.y) / (6.0f * d + e)), d / (q.x + e), q.x);
		}

		float4 tessFunction( appdata_full v0, appdata_full v1, appdata_full v2 )
		{
			return UnityDistanceBasedTess( v0.vertex, v1.vertex, v2.vertex, _TessMin, _TessMax, _TessValue );
		}


		void vertexDataFunc( inout appdata_full v )
		{
            float2 uv_DispMap = v.texcoord* DispMap_ST.xy + DispMap_ST.zw;
            float3 hsvTorgb2 = RGBToHSV( (DispMap_On * DispMap_Scale * tex2Dlod( DispMap, float4( uv_DispMap, 0, 0.0f) )).rgb );
            float3 temp_cast_1 = (hsvTorgb2.z).xxx;
            v.vertex.xyz += hsvTorgb2.z * v.normal;
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			float3 uv_aSMA = float3(i.uv_texcoord, 0);
			float3 uv_CD4 = float3(i.uv_texcoord, 1);
			float3 uv_CD8 = float3(i.uv_texcoord, 2);
			float3 uv_CD31 = float3(i.uv_texcoord, 3);
			float3 uv_CD44 = float3(i.uv_texcoord, 4);
			float3 uv_CD45 = float3(i.uv_texcoord, 5);
			float3 uv_CD68 = float3(i.uv_texcoord, 6);
			float3 uv_CK7 = float3(i.uv_texcoord, 7);
			float3 uv_CK19 = float3(i.uv_texcoord, 8);
			float3 uv_DAPI = float3(i.uv_texcoord, 9);
			float3 uv_Ecadherin = float3(i.uv_texcoord, 10);
			float3 uv_Estrogen_Receptor = float3(i.uv_texcoord, 11);
			float3 uv_Her2 = float3(i.uv_texcoord, 12);
			float3 uv_Ki67 = float3(i.uv_texcoord, 13);
			float3 uv_LaminAC = float3(i.uv_texcoord, 14);
			float3 uv_PCNA = float3(i.uv_texcoord, 15);
			float3 uv_pHH3 = float3(i.uv_texcoord, 16);
			float3 uv_Vimentin = float3(i.uv_texcoord, 17);
			o.Emission = ((((aSMA_On * aSMA_Color.xyz * UNITY_SAMPLE_TEX2DARRAY(_AAAAAAAAAAAA, uv_aSMA).rgb)  + (Her2_On * Her2_Color.xyz * UNITY_SAMPLE_TEX2DARRAY(_AAAAAAAAAAAA, uv_Her2).rgb)) +  ((CD4_On * CD4_Color.xyz * UNITY_SAMPLE_TEX2DARRAY(_AAAAAAAAAAAA, uv_CD4).rgb)  + (CD8_On * CD8_Color.xyz * UNITY_SAMPLE_TEX2DARRAY(_AAAAAAAAAAAA, uv_CD8).rgb)))  + (((CD31_On * CD31_Color.xyz * UNITY_SAMPLE_TEX2DARRAY(_AAAAAAAAAAAA, uv_CD31).rgb) + (CD45_On * CD45_Color.xyz * UNITY_SAMPLE_TEX2DARRAY(_AAAAAAAAAAAA, uv_CD45).rgb))  + ((CD68_On * CD68_Color.xyz * UNITY_SAMPLE_TEX2DARRAY(_AAAAAAAAAAAA, uv_CD68).rgb) + (CK7_On * CK7_Color.xyz * UNITY_SAMPLE_TEX2DARRAY(_AAAAAAAAAAAA, uv_CK7).rgb))))  + (((((CK19_On * CK19_Color.xyz * UNITY_SAMPLE_TEX2DARRAY(_AAAAAAAAAAAA, uv_CK19).rgb)  + (DAPI_On * DAPI_Color.xyz * UNITY_SAMPLE_TEX2DARRAY(_AAAAAAAAAAAA, uv_DAPI).rgb)) + ((Ecadherin_On * Ecadherin_Color.xyz * UNITY_SAMPLE_TEX2DARRAY(_AAAAAAAAAAAA, uv_Ecadherin).rgb)  + (Estrogen_Receptor_On * Estrogen_Receptor_Color.xyz * UNITY_SAMPLE_TEX2DARRAY(_AAAAAAAAAAAA, uv_Estrogen_Receptor).rgb))) + (((Ki67_On * Ki67_Color.xyz * UNITY_SAMPLE_TEX2DARRAY(_AAAAAAAAAAAA, uv_Ki67).rgb) + (LaminAC_On * LaminAC_Color.xyz * UNITY_SAMPLE_TEX2DARRAY(_AAAAAAAAAAAA, uv_LaminAC).rgb)) + ((PCNA_On * PCNA_Color.xyz * UNITY_SAMPLE_TEX2DARRAY(_AAAAAAAAAAAA, uv_PCNA).rgb)  + (pHH3_On * pHH3_Color.xyz * UNITY_SAMPLE_TEX2DARRAY(_AAAAAAAAAAAA, uv_pHH3).rgb)))) + (((Vimentin_On * Vimentin_Color.xyz * UNITY_SAMPLE_TEX2DARRAY(_AAAAAAAAAAAA, uv_Vimentin).rgb)) + (CD44_On * CD44_Color.xyz * UNITY_SAMPLE_TEX2DARRAY(_AAAAAAAAAAAA, uv_CD44).rgb)));
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	// CustomEditor "ASEMaterialInspector"
}
