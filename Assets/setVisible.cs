﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class setVisible : MonoBehaviour
{
    public colorCounter parent;
    public void SetVisibility(bool isOn){
        if(isOn){
            gameObject.SetActive(true);
            parent.count++;
            parent.mostRecent = gameObject.transform.parent.gameObject.name;
        } else {
            gameObject.SetActive(false);
            parent.count--;
        }
    }
    void Update()
    {
        if(parent.count > 1 && !gameObject.transform.parent.gameObject.name.Equals(parent.mostRecent)){
            gameObject.SetActive(false);
            parent.count = parent.count - 1;
        }
    }
}
