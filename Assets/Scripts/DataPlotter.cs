﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Animations;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class DataPlotter : MonoBehaviour
{


    private ParticleSystem.Particle[] particlePoints;
    public Slider labelScaler;
    public SteamVR_Action_Boolean grab;
    private GameObject xAxis;
    private GameObject xAxisLabel;
    private GameObject yAxis;
    float labelScale = 0.005f;
    private GameObject yAxisLabel;
    private GameObject zAxis;
    private GameObject zAxisLabel;
    public TMP_Dropdown DropdownX;
    public TMP_Dropdown DropdownY;
    public TMP_Dropdown DropdownZ;
    public TMP_Dropdown DropdownR;
    public TMP_Dropdown DropdownG;
    public TMP_Dropdown DropdownB;
    public string inputfile;
    public VRInputModule input;
    public Camera mainCamera;
    // List for holding data from CSV reader
    public List<Dictionary<string, object>> pointList;
        // Indices for columns to be assigned
    public int columnX = 33;
    public int columnY = 35;
    public int columnZ = 26;
    public int columnR = 33;
    public int columnG = 35;
    public int columnB = 26;
    public GameObject HUD;


    public UnityEngine.UI.Toggle LabelToggle;
    public GameObject AxisPrefab;
    public string xName;
    public string yName;
    public string zName;
    public string rName;
    public string gName;
    public string bName;
    public GameObject PointPrefab;
    public GameObject scatterPlot;
    private GameObject instance;
    public float plotScale = 0.5f;
    public GameObject TooltipPrefab;
    public GameObject pointerCam;
    public GameObject socket;
    public List<string> columnList;
    private List<GameObject> instantiated = new List<GameObject>();

    public Quaternion oldControllerRot = Quaternion.identity;





    void Start () {

        // Set pointlist to results of function Reader with argument inputfile
        pointList = CSVReader.Read(inputfile);

        //Log to console
        Debug.Log(pointList.Count);
        // Declare list of strings, fill with keys (column names)
        columnList = new List<string>(pointList[1].Keys);

        // Print number of keys (using .count)
        Debug.Log("There are " + columnList.Count + " columns in CSV");

        // Assign column name from columnList to Name variables
        xName = columnList[columnX];
        yName = columnList[columnY];
        zName = columnList[columnZ];
        rName = columnList[columnR];
        gName = columnList[columnG];
        bName = columnList[columnB];

        DropdownX.ClearOptions();
        DropdownY.ClearOptions();
        DropdownZ.ClearOptions();
        DropdownR.ClearOptions();
        DropdownG.ClearOptions();
        DropdownB.ClearOptions();
        DropdownX.AddOptions(columnList);
        DropdownY.AddOptions(columnList);
        DropdownZ.AddOptions(columnList);
        DropdownR.AddOptions(columnList);
        DropdownG.AddOptions(columnList);
        DropdownB.AddOptions(columnList);
        DropdownX.value = columnX;
        DropdownY.value = columnY;
        DropdownZ.value = columnZ;
        DropdownR.value = columnR;
        DropdownG.value = columnG;
        DropdownB.value = columnB;
        DropdownX.onValueChanged.AddListener(delegate {ChangeXAxis(DropdownX.value);});
        DropdownY.onValueChanged.AddListener(delegate {ChangeYAxis(DropdownY.value);});
        DropdownZ.onValueChanged.AddListener(delegate {ChangeZAxis(DropdownZ.value);});
        DropdownR.onValueChanged.AddListener(delegate {ChangeRAxis(DropdownR.value);});
        DropdownG.onValueChanged.AddListener(delegate {ChangeGAxis(DropdownG.value);});
        DropdownB.onValueChanged.AddListener(delegate {ChangeBAxis(DropdownB.value);});
        RenderPlot(xName, yName, zName, rName, gName, bName);
    }

    public void ChangeXAxis(int column)
    {
        for (int i = 0; i < instantiated.Count; i++)
        {
            Destroy(instantiated[i]);
        }
        Destroy(xAxis);
        Destroy(yAxis);
        Destroy(zAxis);
        Destroy(xAxisLabel);
        Destroy(yAxisLabel);
        Destroy(zAxisLabel);
        xName = columnList[column];
        Debug.Log("x changed");
        RenderPlot(xName, yName, zName, rName, gName, bName);
    }
    public void ChangeYAxis(int column)
    {
        for (int i = 0; i < instantiated.Count; i++)
        {
            Destroy(instantiated[i]);
        }
        Destroy(xAxis);
        Destroy(yAxis);
        Destroy(zAxis);
        Destroy(xAxisLabel);
        Destroy(yAxisLabel);
        Destroy(zAxisLabel);
        yName = columnList[column];
        RenderPlot(xName, yName, zName, rName, gName, bName);
    }
    public void ChangeZAxis(int column)
    {
        for (int i = 0; i < instantiated.Count; i++)
        {
            Destroy(instantiated[i]);
        }
        Destroy(xAxis);
        Destroy(yAxis);
        Destroy(zAxis);
        Destroy(xAxisLabel);
        Destroy(yAxisLabel);
        Destroy(zAxisLabel);
        zName = columnList[column];
        RenderPlot(xName, yName, zName, rName, gName, bName);
    }
    public void ChangeRAxis(int column)
    {
        for (int i = 0; i < instantiated.Count; i++)
        {
            Destroy(instantiated[i]);
        }
        Destroy(xAxis);
        Destroy(yAxis);
        Destroy(zAxis);
        Destroy(xAxisLabel);
        Destroy(yAxisLabel);
        Destroy(zAxisLabel);
        rName = columnList[column];
        RenderPlot(xName, yName, zName, rName, gName, bName);
    }
    public void ChangeGAxis(int column)
    {
        for (int i = 0; i < instantiated.Count; i++)
        {
            Destroy(instantiated[i]);
        }
        Destroy(xAxis);
        Destroy(yAxis);
        Destroy(zAxis);
        Destroy(xAxisLabel);
        Destroy(yAxisLabel);
        Destroy(zAxisLabel);
        gName = columnList[column];
        RenderPlot(xName, yName, zName, rName, gName, bName);
    }
    public void ChangeBAxis(int column)
    {
        for (int i = 0; i < instantiated.Count; i++)
        {
            Destroy(instantiated[i]);
        }
        Destroy(xAxis);
        Destroy(yAxis);
        Destroy(zAxis);
        Destroy(xAxisLabel);
        Destroy(yAxisLabel);
        Destroy(zAxisLabel);
        bName = columnList[column];
        RenderPlot(xName, yName, zName, rName, gName, bName);
    }

    private float FindMaxValue(string columnName)
    {
        //set initial value to first value
        float maxValue = Convert.ToSingle(pointList[0][columnName]);

        //Loop through Dictionary, overwrite existing maxValue if new value is larger
        for (var i = 0; i < pointList.Count; i++)
        {
            if (maxValue < Convert.ToSingle(pointList[i][columnName]))
                maxValue = Convert.ToSingle(pointList[i][columnName]);

        }
        //Spit out the max value
        return maxValue;
    }
    private float FindMinValue(string columnName)
    {

        float minValue = Convert.ToSingle(pointList[0][columnName]);

        //Loop through Dictionary, overwrite existing minValue if new value is smaller
        for (var i = 0; i < pointList.Count; i++)
        {
            if (Convert.ToSingle(pointList[i][columnName]) < minValue)
                minValue = Convert.ToSingle(pointList[i][columnName]);
        }

        return minValue;
    }
    public void RenderPlot(string xName, string yName, string zName, string rName, string gName, string bName){
        Debug.Log(xName + " " + yName + " " + zName);
        // Get maxes of each axis
        float xMax = FindMaxValue(xName);
        float yMax = FindMaxValue(yName);
        float zMax = FindMaxValue(zName);
        float rMax = FindMaxValue(rName);
        float gMax = FindMaxValue(gName);
        float bMax = FindMaxValue(bName);
        // Get minimums of each axis
        float xMin = FindMinValue(xName);
        float yMin = FindMinValue(yName);
        float zMin = FindMinValue(zName);
        float rMin = FindMinValue(rName);
        float gMin = FindMinValue(gName);
        float bMin = FindMinValue(bName);
        int rowCount = pointList.Count;
        particlePoints = new ParticleSystem.Particle[rowCount];
        for (var i = 0; i < pointList.Count; i++)
        {
            // Get value in pointList at ith "row", in "column" Name
            float x, y, z, r, g, b;
            if(xMin < 0){
                x =
                (Convert.ToSingle(pointList[i][xName]) - xMin) / (xMax * 2);
            } else {
                x =
                (Convert.ToSingle(pointList[i][xName]) - xMin) / (xMax);
            }
            if(yMin < 0){
                y =
                (Convert.ToSingle(pointList[i][yName]) - yMin) / (yMax * 2);
            } else {
                y =
                (Convert.ToSingle(pointList[i][yName]) - yMin) / (yMax);
            }
            if(zMin < 0){
                z =
                (Convert.ToSingle(pointList[i][zName]) - zMin) / (zMax * 2);
            } else {
                z =
                (Convert.ToSingle(pointList[i][zName]) - zMin) / (zMax);
            }
            if(rMin < 0){
                r =
                (Convert.ToSingle(pointList[i][rName]) - rMin) / (rMax * 2);
            } else {
                r =
                (Convert.ToSingle(pointList[i][rName]) - rMin) / (rMax);
            }
            if(gMin < 0){
                g =
                (Convert.ToSingle(pointList[i][gName]) - gMin) / (gMax * 2);
            } else {
                g =
                (Convert.ToSingle(pointList[i][gName]) - gMin) / (gMax);
            }
            if(xMin < 0){
                b =
                (Convert.ToSingle(pointList[i][bName]) - bMin) / (bMax * 2);
            } else {
                b =
                (Convert.ToSingle(pointList[i][bName]) - bMin) / (bMax);
            }
            string dataPointName =
            xName + ": " + pointList[i][xName] + ", "
            + yName + ": " + pointList[i][yName] + ", "
            + zName + ": " + pointList[i][zName];
            particlePoints[i].position = new Vector3(x, y, z) * plotScale;
            // Set point color
            particlePoints[i].startColor = new Color(r, g, b, 1.0f);
            particlePoints[i].startSize = 0.005f;
        }
        xAxis = Instantiate(AxisPrefab, Vector3.zero, Quaternion.identity, scatterPlot.transform);
        xAxis.transform.localScale = new Vector3(plotScale, 0.005f, 0.005f);
        xAxis.transform.localRotation = Quaternion.identity;
        xAxis.transform.localPosition = new Vector3(plotScale/2.0f+0.0025f, yMin < 0 ? plotScale/2.0f : 0.0f, xMin < 0 ? plotScale/2.0f : 0.0f);
        xAxis.GetComponent<Renderer>().material.color =
            new Color(1.0f, 0.0f, 0.0f, 1.0f);
        xAxisLabel = xAxis.transform.GetChild(0).gameObject;
        xAxisLabel.transform.SetParent(scatterPlot.transform);
        xAxisLabel.GetComponent<FollowCamera>().cameraToLookAt = mainCamera;
        xAxisLabel.transform.localScale = new Vector3(labelScale, labelScale, labelScale);
        xAxisLabel.GetComponent<TextMeshPro>().text = xName + " (" + xMax + ")";
        xAxisLabel.transform.localPosition = new Vector3(plotScale + 0.01f, yMin < 0 ? plotScale/2.0f : 0.0f, xMin < 0 ? plotScale/2.0f : 0.0f);
        yAxis = Instantiate(AxisPrefab, Vector3.zero, Quaternion.identity, scatterPlot.transform);
        yAxis.transform.localRotation = Quaternion.identity;
        yAxis.transform.localScale = new Vector3(0.005f, plotScale, 0.005f);
        yAxis.transform.localPosition = new Vector3(xMin < 0 ? plotScale/2.0f : 0.0f, plotScale/2.0f+0.0025f, zMin < 0 ? plotScale/2.0f : 0.0f);
        yAxis.GetComponent<Renderer>().material.color =
            new Color(0.0f, 1.0f, 0.0f, 1.0f);
        yAxisLabel = yAxis.transform.GetChild(0).gameObject;
        yAxisLabel.transform.SetParent(scatterPlot.transform);
        yAxisLabel.GetComponent<FollowCamera>().cameraToLookAt = mainCamera;
        yAxisLabel.transform.localScale = new Vector3(labelScale, labelScale, labelScale);
        yAxisLabel.GetComponent<TextMeshPro>().text = yName + " (" + yMax + ")";
        yAxisLabel.transform.localPosition = new Vector3(xMin < 0 ? plotScale/2.0f : 0.0f, plotScale + 0.01f, zMin < 0 ? plotScale/2.0f : 0.0f);
        zAxis = Instantiate(AxisPrefab, Vector3.zero, Quaternion.identity, scatterPlot.transform);
        zAxis.transform.localScale = new Vector3(0.005f, 0.005f, plotScale);
        zAxis.transform.localRotation = Quaternion.identity;
        zAxis.transform.localPosition = new Vector3(xMin < 0 ? plotScale/2.0f : 0.0f, yMin < 0 ? plotScale/2.0f : 0.0f, plotScale/2.0f+0.0025f);
        zAxis.GetComponent<Renderer>().material.color =
            new Color(0.0f, 0.0f, 1.0f, 1.0f);
        zAxisLabel = zAxis.transform.GetChild(0).gameObject;
        zAxisLabel.transform.SetParent(scatterPlot.transform);
        zAxisLabel.GetComponent<FollowCamera>().cameraToLookAt = mainCamera;
        zAxisLabel.transform.localScale = new Vector3(labelScale, labelScale, labelScale);
        zAxisLabel.GetComponent<TextMeshPro>().text = zName + " (" + zMax + ")";
        zAxisLabel.transform.localPosition = new Vector3(xMin < 0 ? plotScale/2.0f : 0.0f, yMin < 0 ? plotScale/2.0f : 0.0f, plotScale + 0.01f);
        scatterPlot.GetComponent<Interactable>().hideHighlight[0] = xAxisLabel;
        scatterPlot.GetComponent<Interactable>().hideHighlight[1] = yAxisLabel;
        scatterPlot.GetComponent<Interactable>().hideHighlight[2] = zAxisLabel;
        scatterPlot.GetComponent<ParticleSystem>().SetParticles(particlePoints, particlePoints.Length);




    }
    void Update(){
        CheckInputs(columnX, columnY, columnZ);
    }
    public void CheckInputs(int columnX, int columnY, int columnZ){
        if (!LabelToggle.isOn)
        {
            xAxisLabel.GetComponent<Renderer>().enabled = false;
            yAxisLabel.GetComponent<Renderer>().enabled = false;
            zAxisLabel.GetComponent<Renderer>().enabled = false;
        } else {
            xAxisLabel.GetComponent<Renderer>().enabled = true;
            yAxisLabel.GetComponent<Renderer>().enabled = true;
            zAxisLabel.GetComponent<Renderer>().enabled = true;
        }
        labelScale = labelScaler.value;
        xAxis.transform.localScale = new Vector3(plotScale, 0.005f / scatterPlot.transform.localScale.y, 0.005f / scatterPlot.transform.localScale.z);
        xAxisLabel.transform.localScale = new Vector3(labelScale / scatterPlot.transform.localScale.x, labelScale / scatterPlot.transform.localScale.y, labelScale / scatterPlot.transform.localScale.z);
        yAxis.transform.localScale = new Vector3(0.005f / scatterPlot.transform.localScale.y, plotScale, 0.005f / scatterPlot.transform.localScale.z);
        yAxisLabel.transform.localScale = new Vector3(labelScale / scatterPlot.transform.localScale.x, labelScale / scatterPlot.transform.localScale.y, labelScale / scatterPlot.transform.localScale.z);
        zAxis.transform.localScale = new Vector3(0.005f / scatterPlot.transform.localScale.y, 0.005f / scatterPlot.transform.localScale.z, plotScale);
        zAxisLabel.transform.localScale = new Vector3(labelScale / scatterPlot.transform.localScale.x, labelScale / scatterPlot.transform.localScale.y, labelScale / scatterPlot.transform.localScale.z);

        if (Vector3.Distance(scatterPlot.transform.position, socket.transform.position) < 0.25f && grab.stateUp)
        {
            scatterPlot.transform.SetParent(this.transform);
            scatterPlot.transform.localPosition = Vector3.zero;
            scatterPlot.transform.localRotation = Quaternion.identity;
            scatterPlot.transform.localScale = Vector3.one;
            socket.SetActive(false);

        } else if (Vector3.Distance(scatterPlot.transform.position, socket.transform.position) < 0.25f && !socket.activeInHierarchy && scatterPlot.transform.parent != this.transform && grab.state)
        {
            socket.SetActive(true);
        } else if (!grab.state || Vector3.Distance(scatterPlot.transform.position, socket.transform.position) >= 0.25f)
        {
            socket.SetActive(false);
        }
    }

}
