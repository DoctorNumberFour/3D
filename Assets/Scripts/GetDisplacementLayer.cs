﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetDisplacementLayer : MonoBehaviour
{
    public Text nameField;
    public Texture2D dispTex;
    // Start is called before the first frame update
    void Start()
    {
        nameField = this.GetComponent<Text>();
        Shader.SetGlobalTexture("DispMap", dispTex);
    }

    // Update is called once per frame
    void Update()
    {
        nameField.text = "Heightmap: " + Shader.GetGlobalTexture("DispMap").name.Substring(9) + (Shader.GetGlobalFloat("DispMap_On") == 1.0f ? " (on)" : " (off)");
    }
}
