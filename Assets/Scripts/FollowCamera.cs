﻿using UnityEngine;
using System.Collections;

public class FollowCamera : MonoBehaviour

{

    public Camera cameraToLookAt;

    void Start()
    {
        //transform.Rotate( 180,0,0 );
    }

    void Update()
    {
        transform.LookAt(cameraToLookAt.transform);
        transform.Rotate(0.0f, 180.0f, 0.0f);
    }
}