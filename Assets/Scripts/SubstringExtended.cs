using System;
using System.Collections.Generic;

namespace stringExtended{
    public static class SubstringExtended{
        public static string SubstringPlus(this string str, int startIndex, int endIndex){
            if(endIndex < 0){
                endIndex = str.Length + (endIndex - 1);
            }
            if(startIndex < 0){
                startIndex = str.Length + (startIndex - 1);
            }
            if(endIndex == startIndex){
                return str[endIndex].ToString();
            } else if(endIndex > startIndex){
                string sub = "";
                for(int i = 0; i < str.Length; i++){
                    if(i <= endIndex && i >= startIndex){
                        sub += str[i];
                    }
                }
                return sub;
            } else if(endIndex < startIndex){
                string sub = "";
                for(int i = str.Length - 1; i >= 0; i--){
                    if(i >= endIndex && i <= startIndex){
                        sub += str[i];
                    }
                }
                return sub;
            } else {
                return "what the fuck";
            }

        }
    }
}