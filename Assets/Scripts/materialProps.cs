﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class materialProps : MonoBehaviour
{
    public Text minNum;
    public Text maxNum;
    public Text valNum;
    void Start()
    {
        Shader.SetGlobalFloat("_TessValue", 8f);
        Shader.SetGlobalFloat("_TessMin", 0f);
        Shader.SetGlobalFloat("_TessMax", 30f);
    }
    void OnGUI()
    {
    if (Event.current.Equals(Event.KeyboardEvent(KeyCode.Q.ToString())))
        {
            Shader.SetGlobalFloat("_TessMin", Shader.GetGlobalFloat("_TessMin") - 1);
        } else if (Event.current.Equals(Event.KeyboardEvent(KeyCode.E.ToString())))
        {
            Shader.SetGlobalFloat("_TessMin", Shader.GetGlobalFloat("_TessMin") + 1);
        } else if (Event.current.Equals(Event.KeyboardEvent(KeyCode.A.ToString())))
        {
            Shader.SetGlobalFloat("_TessMax", Shader.GetGlobalFloat("_TessMax") - 1);
        } else if (Event.current.Equals(Event.KeyboardEvent(KeyCode.D.ToString())))
        {
            Shader.SetGlobalFloat("_TessMax", Shader.GetGlobalFloat("_TessMax") + 1);
        } else if (Event.current.Equals(Event.KeyboardEvent(KeyCode.Z.ToString())))
        {
            Shader.SetGlobalFloat("_TessValue", Shader.GetGlobalFloat("_TessValue") - 1);
        } else if (Event.current.Equals(Event.KeyboardEvent(KeyCode.C.ToString())))
        {
            Shader.SetGlobalFloat("_TessValue", Shader.GetGlobalFloat("_TessValue") + 1);
        }
    }
    void Update() {
        minNum.text = "min: " + Shader.GetGlobalFloat("_TessMin");
        maxNum.text = "max: " + Shader.GetGlobalFloat("_TessMax");
        valNum.text = "val: " + Shader.GetGlobalFloat("_TessValue");
    }
}
