﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorScript : MonoBehaviour
{
    public GameObject plane;
    
    private MeshRenderer renderer;
    

    public void OnClickColor(){
        
        renderer = plane.GetComponent<MeshRenderer>();
        renderer.enabled = !renderer.enabled;
    }
}
