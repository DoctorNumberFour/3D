﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DispScaler : MonoBehaviour
{

    public Slider DispScaleSlider;
    private float newScale = 0.5f;
    private float oldScale = 0.5f;
    // Update is called once per frame
    void Update()
    {
        newScale = DispScaleSlider.value;
        if (newScale != oldScale){
            Shader.SetGlobalFloat("DispMap_Scale", newScale);
            oldScale = newScale;
        }
    }
}
