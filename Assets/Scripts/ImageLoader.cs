﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ImageLoader : MonoBehaviour {
    public GameObject plane;

    private MeshRenderer renderer;

    private Material material;
    public Texture2D DAPI_R2;
    public Texture2D PCNA;
    public Texture2D Her2;
    public Texture2D ER;
    public Texture2D DAPI_R3;
    public Texture2D CD45;
    public Texture2D Ki67;
    public Texture2D CK14;
    public Texture2D CD44;
    public Texture2D CK5;
    public Texture2D DAPI_R4;
    public Texture2D Vim;
    public Texture2D PD1;
    public Texture2D LaminAC;
    public Texture2D DAPI_R5;
    public Texture2D aSmA;
    public Texture2D CD68;
    public Texture2D PH3;
    public Texture2D Ecad;
    public static Texture2D ToTexture2D(Texture texture) {
        Texture2D twodee = Texture2D.CreateExternalTexture(texture.width, texture.height, TextureFormat.RGB24, false, false, texture.GetNativeTexturePtr());
        Debug.Log("texture converted");
        return twodee;
    }
    public void Start () {
        renderer = plane.GetComponent<MeshRenderer>();
        Debug.Log("material loaded");
        Debug.Log(renderer.material.mainTexture);
    }
    public void ChangeMaterial (Material material) {
        renderer.material = material;
        Debug.Log(renderer.material.mainTexture);
    }

    public void AddMaterial(Material material, Texture2D layer) {
        Color[] bmap;
        if(material.mainTexture != null){
            bmap = (ToTexture2D(material.mainTexture)).GetPixels();
        } else {
            Texture2D texture2d = new Texture2D((int)(renderer.bounds.size.x*1000.0f), (int)(renderer.bounds.size.z*1000.0f), TextureFormat.ARGB32, false);
            bmap = new Color[texture2d.GetPixels().Length];
            for (int i = 0; i < bmap.Length; i++)
            {
                bmap[i] = new Color(0.0f, 0.0f, 0.0f, 1.0f);
            }
        }
        Color[] add = layer.GetPixels();
        Color c;
        Color d;
        for (int i = 0; i < bmap.Length; i++) {
            c = bmap[i];
            d = add[i];
            float nPixelR = c.r + d.r;
            float nPixelG = c.g + d.g;
            float nPixelB = c.b + d.b;
            bmap[i] = new Color (nPixelR, nPixelG, nPixelB, 1.0f);
        }
        Texture2D toSet = new Texture2D (layer.width, layer.height);
        toSet.SetPixels (bmap);
        toSet.Apply ();
        material.SetTexture ("_MainTex", toSet);
    }
    public void RemoveMaterial (Material material, Texture2D layer) {
        Color[] bmap;
        if(material.mainTexture != null){
            bmap = (ToTexture2D(material.mainTexture)).GetPixels ();
        } else {
            Texture2D texture2d = new Texture2D((int)(renderer.bounds.size.x*1000.0f), (int)(renderer.bounds.size.z*1000.0f), TextureFormat.ARGB32, false);
            bmap = new Color[texture2d.GetPixels().Length];
            for (int i = 0; i < bmap.Length; i++)
            {
                bmap[i] = new Color(0.0f, 0.0f, 0.0f, 1.0f);
            }
        }
        Color[] sub = layer.GetPixels ();
        Color c;
        Color d;
        for (int i = 0; i < bmap.Length; i++) {
            c = bmap[i];
            d = sub[i];
            float nPixelR = c.r + d.r;
            float nPixelG = c.g + d.g;
            float nPixelB = c.b + d.b;
            bmap[i] = new Color (nPixelR, nPixelG, nPixelB, 1.0f);
        }
        Texture2D toSet = new Texture2D (layer.width, layer.height);
        toSet.SetPixels(bmap);
        toSet.Apply();
        material.SetTexture("_MainTex", toSet);
    }
    public void AddDAPI_R2() {
        material = renderer.material;
        Debug.Log(renderer.material.mainTexture);
        AddMaterial(material, DAPI_R2);
        ChangeMaterial(material);
    }
}
