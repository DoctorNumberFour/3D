﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveSpecimen : MonoBehaviour
{
    public GameObject head;
    
    // Start is called before the first frame update
    void Start()
    {
        transform.SetPositionAndRotation(head.transform.position, head.transform.rotation);
        transform.LookAt(head.transform);
        transform.Rotate(new Vector3(0.0f, 180.0f, 0.0f));
        transform.localPosition += new Vector3(0.0f, 0.0f, 2.0f);
        
    }
}
