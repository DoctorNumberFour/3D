// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "DispBase"

	Properties
	{
		_TessValue( "Max Tessellation", Range( 1, 32 ) ) = 32
		_TessMin( "Tess Min Distance", Float ) = 10
		_TessMax( "Tess Max Distance", Float ) = 25
		_TessPhongStrength( "Phong Tess Strength", Range( 0, 1 ) ) = 0.5
		// _DAPI_R2("DAPI R2", 2D) = "black" {}
		// _CD44("CD44", 2D) = "black" {}
		// _PD1("PD1", 2D) = "black" {}
		// _DAPI_R3("DAPI R3", 2D) = "black" {}
		// _Her2("Her2", 2D) = "black" {}
		// _DAPI_R4("DAPI R4", 2D) = "black" {}
		// _DAPI_R5("DAPI R5", 2D) = "black" {}
		// _Ki67("Ki67", 2D) = "black" {}
		// _PH3("PH3", 2D) = "black" {}
		// _CK5("CK5", 2D) = "black" {}
		// _PCNA("PCNA", 2D) = "black" {}
		// _CD45("CD45", 2D) = "black" {}
		// _LaminAC("LaminAC", 2D) = "black" {}
		// _CD68("CD68", 2D) = "black" {}
		// _Vim("Vim", 2D) = "black" {}
		// _ER("ER", 2D) = "black" {}
		DispMap("Disp Map", 2D) = "black" {}
		// _aSmA("aSmA", 2D) = "black" {}
		_Black("Black", 2D) = "black" {}
		// _CK14("CK14", 2D) = "black" {}
		// _Ecad("Ecad", 2D) = "black" {}
		_Boys("Boys", 2DArray) = "" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGPROGRAM
		#include "Tessellation.cginc"
		#pragma target 5.0
		#pragma exclude_renderers vulkan xbox360 xboxone ps4 psp2 n3ds wiiu
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows noshadow vertex:vertexDataFunc tessellate:tessFunction tessphong:_TessPhongStrength 
		#pragma require 2darray
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform int DispMap_On = 0;
		uniform sampler2D DispMap;
		uniform float4 DispMap_ST;

		uniform int DAPI_R2_On = 1;
		uniform float4 DAPI_R2_Color = {0.0, 1.0, 0.0, 1.0};
		// uniform sampler2D _DAPI_R2;
		uniform float4 _DAPI_R2_ST;

		uniform int PCNA_On = 0;
		uniform float4 PCNA_Color = {0.0, 1.0, 1.0, 1.0};
		// uniform sampler2D _PCNA;
		uniform float4 _PCNA_ST;

		uniform int Her2_On = 0;
		uniform float4 Her2_Color = {1.0, 1.0, 0.0, 1.0};
		// uniform sampler2D _Her2;
		uniform float4 _Her2_ST;

		uniform int ER_On = 0;
		uniform float4 ER_Color = {1.0, 0.0, 0.0, 1.0};
		// uniform sampler2D _ER;
		uniform float4 _ER_ST;

		uniform int DAPI_R3_On = 1;
		uniform float4 DAPI_R3_Color = {0.0, 0.0, 1.0, 1.0};
		// uniform sampler2D _DAPI_R3;
		uniform float4 _DAPI_R3_ST;

		uniform int CD45_On = 0;
		uniform float4 CD45_Color = {1.0, 0.0, 1.0, 1.0};
		// uniform sampler2D _CD45;
		uniform float4 _CD45_ST;

		uniform int Ki67_On = 0;
		uniform float4 Ki67_Color = {0.0, 1.0, 0.0, 1.0};
		// uniform sampler2D _Ki67;
		uniform float4 _Ki67_ST;

		uniform int CK14_On = 0;
		uniform float4 CK14_Color = {1.0, 1.0, 0.0, 1.0};
		// uniform sampler2D _CK14;
		uniform float4 _CK14_ST;

		uniform int CD68_On = 0;
		uniform float4 CD68_Color = {0.0, 0.0, 1.0, 1.0};
		// uniform sampler2D _CD68;
		uniform float4 _CD68_ST;

		uniform int PH3_On = 0;
		uniform float4 PH3_Color = {1.0, 0.0, 0.0, 1.0};
		// uniform sampler2D _PH3;
		uniform float4 _PH3_ST;

		uniform int Ecad_On = 0;
		uniform float4 Ecad_Color = {0.0, 0.0, 1.0, 1.0};
		// uniform sampler2D _Ecad;
		uniform float4 _Ecad_ST;

		uniform int CD44_On = 0;
		uniform float4 CD44_Color = {1.0, 0.0, 0.0, 1.0};
		// uniform sampler2D _CD44;
		uniform float4 _CD44_ST;

		uniform int CK5_On = 0;
		uniform float4 CK5_Color = {1.0, 0.0, 1.0, 1.0};
		// uniform sampler2D _CK5;
		uniform float4 _CK5_ST;

		uniform int DAPI_R4_On = 1;
		uniform float4 DAPI_R4_Color = {0.0, 1.0, 1.0, 1.0};
		// uniform sampler2D _DAPI_R4;
		uniform float4 _DAPI_R4_ST;

		uniform int Vim_On = 0;
		uniform float4 Vim_Color = {0.0, 1.0, 0.0, 1.0};
		// uniform sampler2D _Vim;
		uniform float4 _Vim_ST;

		uniform int PD1_On = 0;
		uniform float4 PD1_Color = {0.0, 0.0, 1.0, 1.0};
		// uniform sampler2D _PD1;
		uniform float4 _PD1_ST;

		uniform int LaminAC_On = 0;
		uniform float4 LaminAC_Color = {1.0, 0.0, 1.0, 1.0};
		// uniform sampler2D _LaminAC;
		uniform float4 _LaminAC_ST;

		uniform int DAPI_R5_On = 1;
		uniform float4 DAPI_R5_Color = {1.0, 1.0, 0.0, 1.0};
		// uniform sampler2D _DAPI_R5;
		uniform float4 _DAPI_R5_ST;

		uniform int aSmA_On = 0;
		uniform float4 aSmA_Color = {0.0, 0.0, 1.0, 1.0};
		// uniform sampler2D _aSmA;
		uniform float4 _aSmA_ST;

		uniform float _TessValue;
		uniform float _TessMin;
		uniform float _TessMax;
		uniform float _TessPhongStrength;


		float3 RGBToHSV(float3 c)
		{
			float4 K = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
			float4 p = lerp( float4( c.bg, K.wz ), float4( c.gb, K.xy ), step( c.b, c.g ) );
			float4 q = lerp( float4( p.xyw, c.r ), float4( c.r, p.yzx ), step( p.x, c.r ) );
			float d = q.x - min( q.w, q.y );
			float e = 1.0e-10;
			return float3( abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
		}

		float4 tessFunction( appdata_full v0, appdata_full v1, appdata_full v2 )
		{
			return UnityDistanceBasedTess( v0.vertex, v1.vertex, v2.vertex, _TessMin, _TessMax, _TessValue );
		}
		UNITY_DECLARE_TEX2DARRAY(_Boys);

		void vertexDataFunc( inout appdata_full v )
		{
			float2 uv_DispMap = v.texcoord * DispMap_ST.xy + DispMap_ST.zw;
			float3 hsvTorgb2 = RGBToHSV( (DispMap_On * tex2Dlod( DispMap, float4( uv_DispMap, 0, 0.0) )).rgb );
			float3 temp_cast_1 = (hsvTorgb2.z).xxx;
			v.vertex.xyz += temp_cast_1;
		}

		void surf( Input i , inout SurfaceOutputStandard o )
		
			float3 uv_DAPI_R2 = float3(i.uv_texcoord * _DAPI_R2_ST.xy + _DAPI_R2_ST.zw, 0);
			float3 uv_PCNA = float3(i.uv_texcoord * _PCNA_ST.xy + _PCNA_ST.zw, 1);
			float3 uv_Her2 = float3(i.uv_texcoord * _Her2_ST.xy + _Her2_ST.zw, 2);
			float3 uv_ER = float3(i.uv_texcoord * _ER_ST.xy + _ER_ST.zw, 3);
			float3 uv_DAPI_R3 = float3(i.uv_texcoord * _DAPI_R3_ST.xy + _DAPI_R3_ST.zw, 4);
			float3 uv_CD45 = float3(i.uv_texcoord * _CD45_ST.xy + _CD45_ST.zw, 5);
			float3 uv_Ki67 = float3(i.uv_texcoord * _Ki67_ST.xy + _Ki67_ST.zw, 6);
			float3 uv_CK14 = float3(i.uv_texcoord * _CK14_ST.xy + _CK14_ST.zw, 7);
			float3 uv_CD68 = float3(i.uv_texcoord * _CD68_ST.xy + _CD68_ST.zw, 8);
			float3 uv_PH3 = float3(i.uv_texcoord * _PH3_ST.xy + _PH3_ST.zw, 9);
			float3 uv_Ecad = float3(i.uv_texcoord * _Ecad_ST.xy + _Ecad_ST.zw, 10);
			float3 uv_CD44 = float3(i.uv_texcoord * _CD44_ST.xy + _CD44_ST.zw, 11);
			float3 uv_CK5 = float3(i.uv_texcoord * _CK5_ST.xy + _CK5_ST.zw, 12);
			float3 uv_DAPI_R4 = float3(i.uv_texcoord * _DAPI_R4_ST.xy + _DAPI_R4_ST.zw, 13);
			float3 uv_Vim = float3(i.uv_texcoord * _Vim_ST.xy + _Vim_ST.zw, 14);
			float3 uv_PD1 = float3(i.uv_texcoord * _PD1_ST.xy + _PD1_ST.zw, 15);
			float3 uv_LaminAC = float3(i.uv_texcoord * _LaminAC_ST.xy + _LaminAC_ST.zw, 16);
			float3 uv_DAPI_R5 = float3(i.uv_texcoord * _DAPI_R5_ST.xy + _DAPI_R5_ST.zw, 17);
			float3 uv_aSmA = float3(i.uv_texcoord * _aSmA_ST.xy + _aSmA_ST.zw, 18);
			o.Emission = ( ( ( DAPI_R2_On * DAPI_R2_Color * UNITY_SAMPLE_TEX2DARRAY(_Boys, uv_DAPI_R2) )  + ( PCNA_On * PCNA_Color * UNITY_SAMPLE_TEX2DARRAY(_Boys, uv_PCNA) ) ) + (  Her2_On * Her2_Color * UNITY_SAMPLE_TEX2DARRAY(_Boys, uv_Her2)  ) + ( ER_On * ER_Color * UNITY_SAMPLE_TEX2DARRAY(_Boys, uv_ER) ) )  + ( ( ( DAPI_R3_On * DAPI_R3_Color * UNITY_SAMPLE_TEX2DARRAY(_Boys, uv_DAPI_R3) ) ) + ( CD45_On * CD45_Color * UNITY_SAMPLE_TEX2DARRAY(_Boys, uv_CD45) ) )  + ( ( Ki67_On * Ki67_Color * UNITY_SAMPLE_TEX2DARRAY(_Boys, uv_Ki67) ) ) + ( CK14_On * CK14_Color * UNITY_SAMPLE_TEX2DARRAY(_Boys, uv_CK14) )  + ( CD68_On * CD68_Color * UNITY_SAMPLE_TEX2DARRAY(_Boys, uv_CD68) )  + ( PH3_On * PH3_Color * UNITY_SAMPLE_TEX2DARRAY(_Boys, uv_PH3) ) + ( Ecad_On * Ecad_Color * UNITY_SAMPLE_TEX2DARRAY(_Boys, uv_Ecad) )  + ( ( ( ( CD44_On * CD44_Color * UNITY_SAMPLE_TEX2DARRAY(_Boys, uv_CD44) ) ) + ( CK5_On * CK5_Color * UNITY_SAMPLE_TEX2DARRAY(_Boys, uv_CK5) ) ) ) + ( ( DAPI_R4_On * DAPI_R4_Color * UNITY_SAMPLE_TEX2DARRAY(_Boys, uv_DAPI_R4) ) ) + ( Vim_On * Vim_Color * UNITY_SAMPLE_TEX2DARRAY(_Boys, uv_Vim) )  + ( ( ( PD1_On * PD1_Color * UNITY_SAMPLE_TEX2DARRAY(_Boys, uv_PD1) ) ) + ( LaminAC_On * LaminAC_Color * UNITY_SAMPLE_TEX2DARRAY(_Boys, uv_LaminAC) ) ) + ( ( DAPI_R5_On * DAPI_R5_Color * UNITY_SAMPLE_TEX2DARRAY(_Boys, uv_DAPI_R5) ) ) + ( aSmA_On * aSmA_Color * UNITY_SAMPLE_TEX2DARRAY(_Boys, uv_aSmA) ).rgb;
			o.Alpha = 1;
		

		ENDCG
	
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"

