﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleVisibility : MonoBehaviour
{
    public Toggle toggle;
    // Start is called before the first frame update
    void Start()
    {
        toggle = this.GetComponent<Toggle>();
    }
    public void UpdateVisibility()
    {
        Shader.SetGlobalInt(toggle.gameObject.name.Substring(6), toggle.isOn?1:0);
    }
}
